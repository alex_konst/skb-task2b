const express = require('express');
const app = express();

app.options('/', function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200).send();
});

app.get('/', function (req, res) {
    const re = /[^óa-zA-Zа-яА-Я\s]/g;
    const whitespaceRe = /[\s\x20]+/g;
    const query = req.query;
    const fullnameValue = query.fullname ? query.fullname.trim().replace('\'', '').replace(whitespaceRe, ' ') : '';
    const fullnameArray = (fullnameValue && !re.test(fullnameValue) ? fullnameValue : '').split(' ');
    const lastName = fullnameArray.pop();
    let firstNameAndSurName;
    let fullname = 'Invalid fullname';

    if (fullnameArray.length <= 2 && lastName.length) {
        firstNameAndSurName = fullnameArray.map(val => ` ${val.slice(0, 1).toUpperCase()}.`).join('');
        fullname = lastName.charAt(0).toUpperCase() + lastName.slice(1).toLowerCase() + firstNameAndSurName;
    }

    res.set('Access-Control-Allow-Origin', '*');
    res.send(fullname);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});